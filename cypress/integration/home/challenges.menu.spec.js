/// <reference types="cypress" />

describe('Verify UI Automation Playground home view', () => {
  beforeEach(() => cy.visit('/'));

  const options = [
    'CSS Game',
    'CSS Locators',
    'Querying',
    'Actions',
    'Browser manipulation (WIP)',
    'Common challenges (WIP)',
    'Waiting (WIP)',
    'Network Requests (WIP)',
  ];

  context('Top menu', () => {
    it('should show the correct option in the challenges menu at the top', () => {
      cy.get('.dropdown-toggle').click();
      cy.get('.dropdown-menu li a').then((menuOptions) => {
        menuOptions = menuOptions.toArray();
        expect(menuOptions.length).to.equal(options.length);
        expect(menuOptions.map((option) => option.innerText).every((option) => options.includes(option))).to.be.true;
      });
    });
  });

  context('Home View', () => {
    it('should show correct challenges at the home view', () => {
      cy.get('.home-list li a').then((menuOptions) => {
        menuOptions = menuOptions.toArray();
        expect(menuOptions.length).to.equal(options.length);
        expect(menuOptions.map((option) => option.innerText).every((option) => options.includes(option))).to.be.true;
      });
    });
  });
});
